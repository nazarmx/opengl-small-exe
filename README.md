[![Build status](https://ci.appveyor.com/api/projects/status/js7p54om6wpm677y?svg=true)](https://ci.appveyor.com/project/nazar554/opengl-small-exe)

This is a test how small a OpenGL program can be.
- Uses GLFW3 and GLEW statically linked
- Executable stripped and upxed
- CMake takes care about everything
