set -e 
set -o pipefail
if [[ "$MSYSTEM" == "MINGW64" ]]; then
    arch="x86_64"
elif [[ "$MSYSTEM" == "MINGW32" ]]; then
    arch="i686"
else
    exit 1
fi

mkdir -p $APPVEYOR_BUILD_FOLDER/build-$arch
mkdir -p $APPVEYOR_BUILD_FOLDER/install-$arch
cd $APPVEYOR_BUILD_FOLDER/build-$arch
$MINGW_MOUNT_POINT/bin/cmake \
    -G'MSYS Makefiles' \
    -DCMAKE_BUILD_TYPE=MinSizeRel \
    -DCMAKE_INSTALL_PREFIX=$APPVEYOR_BUILD_FOLDER/install-$arch \
    -DCMAKE_C_COMPILER=$MINGW_MOUNT_POINT/bin/gcc.exe \
    $APPVEYOR_BUILD_FOLDER
make -j3 package
