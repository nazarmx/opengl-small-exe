set -e 
set -o pipefail
for arch in x86_64 i686; do
pacman -S --needed --noconfirm mingw-w64-$arch-gcc \
                                 mingw-w64-$arch-glew \
                                 mingw-w64-$arch-glfw \
                                 mingw-w64-$arch-cmake
done