#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

static const int WINDOW_X = 1024;
static const int WINDOW_Y = 768;

void onError(int code, const char *desc)
{
    fprintf(stderr, "[GLFW Error](%d): %s\n", code, desc);
}
void onKey(GLFWwindow * win, int key, int scancode, int action, int mods)
{
    if( (key == GLFW_KEY_ESCAPE) && (action == GLFW_PRESS) )
        glfwSetWindowShouldClose(win, GL_TRUE);
}
void setupWindowParameters()
{
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);

    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
    glfwWindowHint(GLFW_SAMPLES, 8); // 8xMSAA
}

static GLuint gVAO;
static GLuint gVertices;
static GLuint gShaderProgram;

void onInitialize()
{
    if(!GLEW_ARB_vertex_array_object)
    {
        fputs("[Internal Error]: VAO is not supported (extension GL_ARB_vertex_array_object not found).\n", stderr);
        exit(EXIT_FAILURE);
    }

    glGenVertexArrays(1, &gVAO);
    glBindVertexArray(gVAO);


    gShaderProgram = glCreateProgram();
    GLuint shaders[2] = { glCreateShader(GL_VERTEX_SHADER), glCreateShader(GL_FRAGMENT_SHADER) };

    const char vertexShader[] = "#version 210\n"
            "in vec3 ";

    //glShaderSource()


    glEnableVertexAttribArray(0);
    glGenBuffers(1, &gVertices);
    glBindBuffer(GL_ARRAY_BUFFER, gVertices);

    GLfloat triangleVertices[] = {
        -0.5f, -0.5f, -1.0f,
        0.0f, 0.5f, -1.0f,
        -0.5f, -0.5f, -1.0f
    };

    glBufferData(GL_ARRAY_BUFFER, sizeof(triangleVertices), triangleVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);

}

void onDraw()
{

}

void onCleanup()
{
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glDeleteBuffers(1, &gVertices);
    glDeleteVertexArrays(1, &gVAO);
}

int main(int argc, char *argv[])
{
    if(argc == 1)
    {
        char *logFilename = malloc(strlen(argv[0]) + 1 + 4);
        strcpy(logFilename, argv[0]);
        strcat(logFilename, ".log"); // Will reuse the NULL byte

        printf("[Notice]: stderr will be redericted to %s\n", logFilename);
        puts("[Notice]: To disable this behavior pass any commandline arguments");
        fflush(stdout);

        freopen(logFilename, "a", stderr);
        free(logFilename);
    }

    glfwSetErrorCallback(&onError);

    if(!glfwInit())
        return EXIT_FAILURE;

    setupWindowParameters();
    GLFWwindow *window = glfwCreateWindow(WINDOW_X, WINDOW_Y, "Hello World", NULL, NULL);
    if(!window)
    {
        glfwTerminate();
        return EXIT_FAILURE;
    }
    
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1);
    
    glewExperimental = GL_TRUE;

    GLenum code = glewInit();
    if(code != GLEW_OK)
    {
        fprintf(stderr, "[GLEW Error](%d): %s\n", code, glewGetErrorString(code));
        return EXIT_FAILURE;
    }

    glfwSetKeyCallback(window, onKey);
    onInitialize();
    while(!glfwWindowShouldClose(window))
    {       
       onDraw();

       glfwSwapBuffers(window);
       glfwPollEvents();
    }

    onCleanup();

    glfwDestroyWindow(window);
    glfwTerminate();
}
